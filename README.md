# README #

As a student, most of the content of my bitbucket account cannot be posted publicly to avoid academic integrity violations. However, I will happily share code samples or repos with people who are not students (e.g. people with jobs, recruiters). 

If you want access, send me an email: reidlong@cmu.edu

### 18-643 (Fall 2017) ###

* [Course Information](http://users.ece.cmu.edu/~jhoe/doku/doku.php?id=18-643_reconfigurable_logic)

### 15-316 (Spring 2017) ###

* [Course Information](https://cmu-15-316.github.io)
* Web Server in OCaml

### 15-410 (Fall 2016) ###

* [Course Information](http://www.cs.cmu.edu/~410-f16/projects.html)
* Backtrace Utility
* Minesweeper Kernel game
    * x86 Console Driver
	* x86 Keyboard Driver
	* x86 Timer Driver
* Thread Library (Pthreads)
    * [Specification](http://www.cs.cmu.edu/~410-f16/p2/thr_lib.pdf)
	* Mutex
	* Condition Variable
	* Semaphore
	* Reader/Writer Lock
	* Many other higher order synchronization devices built off of the above primitives 
* Pebbles Kernel (x86 Target)
	* [Specification](http://www.cs.cmu.edu/~410-f16/p2/kspec.pdf)
	* Unix like (~30 system calls)
* PebTrace - Thread debugger kernel extension
    * [Specification](http://www.cs.cmu.edu/~410-f16/p4/index.html)
* PebPeb - Paravirtualized Hypervisor (x86) [TA Project]
    * [Specification](http://www.cs.cmu.edu/~410-s11/p4/p4pebpeb.pdf)
	* Includes virtual consoles (basic tmux)
* PebP1 - Trap-and-Emulate Hypervisor (Subset of x86) [TA Project]
    * [Specification](http://www.cs.cmu.edu/~410-s17/p4/p4_pebp1.pdf)
	* Designed this assignment
	* Both kernel mode flavor and user mode flavor built on top of PebTrace
	* Built with segmentation
	
#### 18-845 (Spring 2017) ###

* [Course Information](http://www.ece.cmu.edu/~ece845/)
* Simple Webserver
    * Multi-threaded
	* Custom dynamic content protocol using dynamic shared objects

### 18-447 (Spring 2017) ###

* [Course Information](http://users.ece.cmu.edu/~jhoe/doku/doku.php?id=18-447_introduction_to_computer_architecture)
* RISCV Architecture
    * C RISCV Simulator
	* Single Cycle CPU
	* 5 Stage Pipeline
	* 5 Stage Pipeline with branch prediction
	* Superscalar 2 parallel 5 stage pipeline design
	
### 18-341 (Fall 2016) ###
Logic Design and Verification

* [Course Information](https://www.ece.cmu.edu/courses/items/18341.html)
* SystemVerilog
	* Matrix Multiply
	* NOC Router
	* USB Protocol Engine
	* DRAM Memory Controller
	* Verification (SystemVerilog concurrent assertion test bench for calculator)

### 18-348 (Spring 2016) ###

* [Course Information](http://www.ece.cmu.edu/~ece348/)

### 15-214 (Fall 2015) ###

* [Course Information](http://www.cs.cmu.edu/~charlie/courses/15-214/2015-fall/)
* Bus Simulator
* Scrabble Game (with GUI)
* Social Network Scrapper
* Map/Reduce Framework

### CIA Buggy Website ###

* [CIABuggy.org](http://ciabuggy.org)

